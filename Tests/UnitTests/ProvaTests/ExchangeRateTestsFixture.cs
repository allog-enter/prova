using Bogus;
using Prova;

namespace ProvaTests;

public class ExchangeRateTestsFixture//Questão-4
{
    public ExchangeRate GenerateValidFactorExchangeRate()//Questão-1
    {
        DateOnly quotationDate = new(1999, 8, 7);
        decimal factor = 0.123456m;
        Guid typeId = Guid.NewGuid();
        Guid currencyOriginId = Guid.NewGuid();
        Guid currencyDestinationId = Guid.NewGuid();

        return new ExchangeRate(quotationDate, factor, typeId, currencyOriginId, currencyDestinationId);
    }

    public ExchangeRate GenerateFactorExchangeRate(decimal value)//Questão-2
    {
        DateOnly quotationDate = DateOnly.FromDateTime(DateTime.Now);
        decimal factor = value;
        Guid typeId = Guid.NewGuid();
        Guid currencyOriginId = Guid.NewGuid();
        Guid currencyDestinationId = Guid.NewGuid();

        return new ExchangeRate(quotationDate, factor, typeId, currencyOriginId, currencyDestinationId);
    }

    public ExchangeRate GenerateInvalidTypeIdExchangeRate_Bogus()//Questão-3
    {
        var faker = new Faker();//Questão-5

        DateOnly quotationDate = DateOnly.FromDateTime(faker.Date.Past());
        decimal factor = faker.Random.Decimal(min: 0.000001m);
        Guid typeId = Guid.Empty;
        Guid currencyOriginId = faker.Random.Guid();
        Guid currencyDestinationId = faker.Random.Guid();

        return new ExchangeRate(quotationDate, factor, typeId, currencyOriginId, currencyDestinationId);
    }

    public ExchangeRate GenerateInvalidQuotationDateExchangeRate_Bogus()//Questão-6
    {
        var faker = new Faker();

        DateOnly quotationDate = DateOnly.FromDateTime(faker.Date.Future());
        decimal factor = faker.Random.Decimal(min: 0.000001m);
        Guid typeId = faker.Random.Guid();
        Guid currencyOriginId = faker.Random.Guid();
        Guid currencyDestinationId = faker.Random.Guid();

        return new ExchangeRate(quotationDate, factor, typeId, currencyOriginId, currencyDestinationId);
    }
}