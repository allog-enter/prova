using FluentAssertions;
using Prova;

namespace ProvaTests;

public class ExchangeRateTests : IClassFixture<ExchangeRateTestsFixture>
{
    private ExchangeRateTestsFixture _fixture;
    public ExchangeRateTests(ExchangeRateTestsFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact(DisplayName = "ExchangeRate Factor arredondar para cinco casas decimais")]
    [Trait("Category", "ExchangeRate Unit Tests")]
    public void ValidateExchangeRate_WhenTheValueHasMoreThanFiveDecimalPlaces_ShouldBeRoundedToOnlyFive()
    {
        //Arrange        
        ExchangeRate exchangeRate = _fixture.GenerateValidFactorExchangeRate();

        //Act
        Action act = () => exchangeRate.Validate();

        //Assert
        Assert.Equal(0.12346m, exchangeRate.Factor);
        act.Should().NotThrow();
    }

    [Theory(DisplayName = "ExchangeRate invalid quando Factor for menor ou igual zero")]
    [Trait("Category", "ExchangeRate Unit Tests")]
    [InlineData(0)]
    [InlineData(-0.123456)]
    public void ValidateExchangeRate_WhenFactorIsInvalid_ShouldThrowDomainExceptions(decimal invalidFactor)
    {
        //Arrange

        //Act        
        Action act = () => _fixture.GenerateFactorExchangeRate(invalidFactor);

        //Assert
        Assert.Throws<DomainException>(act);
    }

    [Fact(DisplayName = "ExchangeRate invalid quando TypeID for vazio BOGUS")]
    [Trait("Category", "ExchangeRate Unit Tests")]
    public void ValidateExchangeRate_WhenTypeIdIsEmpty_ShouldThrowDomainExceptions()
    {
        //Arrange       

        //Act        
        Action act = () => _fixture.GenerateInvalidTypeIdExchangeRate_Bogus();

        //Assert
        Assert.Throws<DomainException>(act);
    }

    [Fact(DisplayName = "ExchangeRate invalid quando QuotationDate for uma data futura BOGUS")]
    [Trait("Category", "ExchangeRate Unit Tests")]
    public void ValidateExchangeRate_WhenQuotationDateIsFutureDate_ShouldThrowDomainExceptions()
    {
        //Arrange       

        //Act        
        Action act = () => _fixture.GenerateInvalidQuotationDateExchangeRate_Bogus();

        //Assert
        Assert.Throws<DomainException>(act);
    }
}